section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy

 
 
; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte[rdi+rax], 0
    je .loop_end
    inc rax
    jmp .loop
.loop_end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит ее в stderr
print_error:
   mov rsi, 2
   jmp print_to

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    mov rsi, 1

; Принимает указатель на нуль-терминированную строку и куда выводить
print_to:
    push rsi
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    pop rdi
    mov rax, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    sub rsp, 24
    mov byte[rdi], 0
.loop:
    xor rdx, rdx
    div rsi
    or dl, 48
    dec rdi
    mov [rdi], dl
    test rax, rax
    jnz .loop
    call print_string
    add rsp, 24
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte[rdi]
    mov cl, byte[rsi]
    inc rdi
    inc rsi
    cmp al, cl
    je .is_end
    xor rax, rax
    ret
  .is_end:
    test al, al
    jnz string_equals
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor r8, r8
    dec rsi
.read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0x20
    je .is_word_end
    cmp rax, 0x9
    je .is_word_end
    cmp rax, 0xA
    je .is_word_end
    mov r8, 1
    jmp .write_char
.is_word_end:
    test r8, r8
    jz .read_char
    jmp .word_end
.write_char:
    cmp rdx, rsi
    ja .too_long
    test al, al
    jz .word_end
    mov byte[rdi+rdx], al
    inc rdx
    jmp .read_char
.word_end:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
.too_long:
    xor rax, rax
    ret

; Работает аналогично read_word, только не пропускает пробельные символы в начале
; Читает всю строку, а не одно слово
read_line:
    xor rdx, rdx
    dec rsi
.read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi
    cmp rax, 0xA
    je .line_end
    test al, al
    jz .line_end
    cmp rdx, rsi
    ja .too_long
    mov byte[rdi+rdx], al
    inc rdx
    jmp .read_char
.line_end:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
.too_long:
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rsi, rsi
    mov r8, 10
    xor r9, r9
.parse_digit:
    mov sil, byte[rdi+r9]
    test sil, sil
    jz .uint_end
    cmp sil, 48
    jb .uint_end
    cmp sil, 57
    ja .uint_end
    sub sil, 48
    inc r9
    mul r8
    add rax, rsi
    jmp .parse_digit
.uint_end:
    mov rdx, r9
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rsi, rsi
    mov sil, byte[rdi]
    cmp sil, '-'
    je .parse_signed
    call parse_uint
    ret
.parse_signed:
    inc rdi
    call parse_uint
    test rdx, rdx
    jnz .not_error
    ret 
.not_error:
    inc rdx
    neg rax
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rdx, rax
    jae .copy
    xor rax, rax
    ret
.copy:
    xor rcx, rcx
    sub rcx, rax
    dec rcx
.loop:
    mov rdx, [rdi]
    mov [rsi], rdx
    inc rdi
    inc rsi
    inc rcx
    test rcx, rcx
    jnz .loop
    ret 
