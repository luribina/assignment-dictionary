%include "lib.inc"
global _start

section .rodata
%include "colon.inc"
%include "words.inc"

word_enter: db "Enter key: ", 0
word_found: db "Value found: ", 0
word_error: db "This entry is not in the list", 0

%define buffer_size 256
%define address_size 8

section .text
_start:
    mov rdi, word_enter
    call print_string
    sub rsp, buffer_size
    mov rdi, rsp
    call read_line
    mov rdi, rax
    mov rsi, last
    call find_word
    test rax, rax
    jz .error
    add rax, address_size
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    inc rdi
    add rdi, rax
    push rdi
    mov rdi, word_found
    call print_string
    pop rdi
    call print_string
    jmp .exit
.error:
    mov rdi, word_error
    call print_error
.exit:
    call print_newline
    add rsp, buffer_size
    xor rdi, rdi
    call exit
